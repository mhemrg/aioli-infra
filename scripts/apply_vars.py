#!/usr/bin/env python3

import yaml
from jinja2 import Template
from pathlib import Path
import tempfile
import os
import sys
import secrets
import string
import hashlib
import datetime
import json
import fnmatch

def get_allVars(test=False):
  """
  Returns all variables in a dict
  test vars will have precedence over prod vars
  """
  allVars = {}
  varFiles = Path('../group_vars/').rglob('*.yml')
  testFiles = []
  for varFile in varFiles:
    if "test" in str(varFile):
      testFiles.append(varFile)
    else:
      currFile = open(varFile, 'r')
      allVars.update(yaml.safe_load(currFile))
      currFile.close()
  # use test vars if test
  if test == True:
    for testFile in testFiles:
      currFile = open(testFile, 'r')
      allVars.update(yaml.safe_load(currFile))
      currFile.close()
  return allVars

def get_target_files():
  """
  Returns all files in current directory in a list
  """
  targetFiles = []
  paths = Path('./').rglob('*')
  for path in paths:
    if path.is_file() and "result" not in str(path.resolve()):
     targetFiles.append(path)
  return targetFiles

def sub_in_dir(files, variables, outDir=tempfile.mkdtemp()):
  """
  Substitutes the supplied variables in all supplied files
  and places them in a temporary directory of which the path is returned
  this path is also available as a variable when templating as tmpDir
  """
  for file in files:
    
    # Substitute varaibles in file    
    subResult = Template(open(file, 'r').read()).render(variables, outDir=outDir)
    # Keep going until all variables are substituted
    while True:
      prevSubResult = subResult
      subResult = Template(subResult).render(variables)
      # if rendering again does nothing, it must mean all variables are substituted
      if prevSubResult == subResult:
       break
    # Full path
    fullPath = "{}/{}".format(outDir, file)

    # Make parent directories
    os.makedirs(os.path.dirname(fullPath), exist_ok=True)

    # 'x' means throw error if it already exists
    # (redundant check to prevent files getting overwritten)
    f = open(fullPath, 'x')
    f.write(subResult)
    f.close()
  return outDir

def gen_pass():
  charset = string.ascii_letters + string.digits
  password = ''.join(secrets.choice(charset) for i in range(64))
  return password

def ins_passwords(variables, key=None, changedVar=''):
  """
  Inserts on-the-fly generated passwords as value if variable key is gen_pass
  and returns that dict of variables
  """
  # Go deeper if variables nested in list
  if type(variables) is list:
    for i, val in enumerate(variables):
      variables[i] = ins_passwords(val, str(i), changedVar + str(i) + '.')
    return variables
  # Go deeper if variables nested in dict
  if type(variables) is dict:
    for key, val in variables.items():
      variables[key] = ins_passwords(val, key, changedVar + key + '.')
    return variables
  if key == None:
    return variables
  if key.startswith("gen_pass"):
    variables = gen_pass()
  return variables

def is_prod():
  if len(sys.argv) <= 1 or sys.argv[1] != "prod":
    return False
  else:
    return True

def is_test():
  if len(sys.argv) <= 1 or sys.argv[1] != "test":
    return False
  else:
    return True

def get_content_of_files(files):
  allContent = ''
  for file in files:
    content = open(file, 'r').read()
    allContent += str(content)
  return allContent

def main():
  v = get_allVars(True) if is_test() else get_allVars()
  f = get_target_files()

  if is_prod():
    d1 = json.dumps(v)
    d2 = get_content_of_files(f)
    d3 = f
    dfinal = (str(d1) + str(d2) + str(d3)).encode('utf-8')
    # SHAKE is faster than SHA3 with same collision-, but not preimage resistance
    # https://en.wikipedia.org/wiki/SHA-3#Speed
    h = hashlib.shake_128(dfinal).hexdigest(16)
    for path in os.listdir('.'):
      if fnmatch.fnmatchcase(path, "result*{}".format(h)):
        # If a result directory based on identical input files exist, return that
        print(path)
        sys.exit()
    t = datetime.datetime.now().replace(microsecond=0).isoformat()
    prodOutDir = "{}/result_{}_{}".format(str(os.getcwd()), t, h)
  ins_passwords(v)
  r =  sub_in_dir(f, v, prodOutDir) if is_prod() else sub_in_dir(f, v)
  print(r)

if __name__== "__main__":
  main()
