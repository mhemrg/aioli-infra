#!/bin/sh
set -e
TMP_DIR_SYMLINK=tmpDir
ln -s $(../scripts/apply_vars.py) $TMP_DIR_SYMLINK
sudo docker swarm init
sudo docker network create --driver=overlay --attachable balance
