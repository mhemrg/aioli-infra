#!/bin/sh
TMP_DIR_SYMLINK=tmpDir
TRASH=$TMP_DIR_SYMLINK

if [ -d $TMP_DIR_SYMLINK ]; then
		rm -r $TMP_DIR_SYMLINK/*
		rm -r $TRASH
	else
		echo $TMP_DIR_SYMLINK not found
fi
sudo docker swarm leave --force || true
