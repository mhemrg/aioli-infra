.DEFAULT_GOAL:= check

.PHONY: syntax ansible-lint check watch yamllint

yamllint:
				yamllint -c .yamllint.yml .

ansible_lint:
				ansible-lint playbook.yml

syntax:
				ansible-playbook playbook.yml  --syntax-check

check:
				make syntax && make ansible-lint && make yamllint

watch:
				find . -name "*.yml" | entr make

