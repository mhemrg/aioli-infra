{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  name = "aioli-infra-env";
  buildInputs = with pkgs; [
    ansible
    ansible-lint
    entr
    gnumake
    vagrant
    findutils
    python3
    python37Packages.yamllint
    python37Packages.pyyaml
    python37Packages.jinja2
    sshpass
  ];
}
