# Docker Stacks

## Modifying/adding a docker stack

A Docker daemon needs to be running.

1. `$ nix-shell` or install packages otherwise, but beware of version incompatibilities.

2. `$ cd stacks`

3. `$ make prep`

4. `$ mkdir $stack` where `$stack` is the name of whichever component is closest to the user.

5. `$ cd $stack` 

6. `$ vim/emacs/heresy stack.yml`

Remember the following wrt. `stack.yml`:

- Use versioned images (ie. `postgres:12` instead of `postgres:latest`).
- Use Docker's network seperation.
- Use `endpoint_mode: dnsrr`, such that HAProxy can autodiscover it.
- Parameterize (ie. `NEXTCLOUD_APPS={{nextcloud.apps}}` instead of `NEXTCLOUD_APPS=news`).
- Avoid mounted volumes (ie. `db:/var/lib/postgresql/data`, `configs:`, and `secrets:` instead of `etc/:/etc:ro`).
- When selecting a base image, Alpine is generally the first choice (due its size), Debian the second (due to being similar to the host OS) and others third. Additionally, hadolint, our Dockerfile linter, is configured with these images in mind.

<!-- (See the nextcloud stack as an example of best practices) -->

7. `make prep; docker stack deploy -c stack.yml $stack` does the service work as expected?

Beware that the name, `$stack` in this example, matters since it is used by the internal DNS servicer in docker and thus by the HAProxy backend configuration.
The name of the stack should always match the name of the directory that it is located in.

If changes are needed that can not be handled in `stack.yml` is needed a Dockerfile can be added to `../../containers/$stack` and `../../.gitlab-ci.yml`.
Note that you can overwrite both `ENTRYPOINT` and `CMD` from `stack.yml` which is quite powerful.
<!-- (see the nextcloud Dockerfile as an example of best practices) -->

8. Add a backend entry to `../haproxy/stack.yml`

9. `docker stack deploy -c ./haproxy/stack.yml haproxy` does the service work as expected with HAProxy?

Be aware that if you recently `docker stack rm`-ed a stack, that a [race condition in docker](https://github.com/moby/moby/issues/29293) can cause issues when creating networks.
A workaround is to wait 5-10 seconds from `docker stack rm` to `docker stack deploy`.

10. `make clean`

11. Submit MR.
