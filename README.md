[Ansible]: https://docs.ansible.com/ansible/latest/index.html
[Docker swarm]: https://docs.docker.com/engine/swarm/
[Compose]: https://docs.docker.com/compose/compose-file/
[HAProxy]: https://www.haproxy.org/
[Dex]: https://github.com/dexidp/dex
[Nextcloud]: https://nextcloud.com/
[GitLab]: https://about.gitlab.com/
[Matrix]: https://matrix.org/
[OpenLDAP]: https://www.openldap.org/
[Riot]: https://about.riot.im/
[Fricloud]: https://fricloud.dk/

# aioli-infra

![aioli logo](logo.svg)
all in one libre infrastructure


## Description
aioli uses [Ansible] to initialize a [Docker swarm] and deploy Docker stacks from stack-compliant [Compose] files.

A simplified network diagram can be seen below.

```mermaid
graph BT
  User -- HTTPS --- Docker

  subgraph "aioli machine 1"
  Docker --- HAProxy
  HAProxy --- service-1_nginx
  HAProxy --- service-2_nginx
  HAProxy --- service-n_nginx
  service-1_nginx -- HTTP --- service-1_app
  service-2_nginx -- HTTP --- service-2_app
  service-n_nginx -- HTTP --- service-n_app
  service-1_app --- service-1_db[("service-1_db")]
  service-2_app --- service-2_db[("service-2_db")]
  service-n_app --- service-n_db[("service-n_db")]
  end
```

### Background
aioli is developed primarily to power [Fricloud].

### Principles

- Be reuseable.

aioli only requires a change of Ansible variables to adapt.

- Be maintainable.

aioli only requires a rerun of Ansible to update and a visit to Grafana to monitor.

### Features [WIP]

- Scales across multiple hosts thanks to Docker swarm and [HAProxy].
- SSO (Single sign-on) thanks to [Dex] and [OpenLDAP].


### User Facing Stacks [WIP]

- [Nextcloud]
- [GitLab]
- [Matrix]
- [Riot]

# Hacking on / developing aioli

There is a README in subdirectories describing how to work on that part of aioli.

- [Docker stacks](stacks/)
- [Runners](runners/)

Root-level documentation is below.

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Project Structure](#project-structure)
- [Development Packages](#development-packages)
    - [Adding a Package Used During Development](#adding-a-package-used-during-development)
- [Hacking Ansible](#hacking-ansible)
    - [Modifying/adding an Ansible role](#modifyingadding-an-ansible-role)
- [Hacking CI](#hacking-ci)
    - [Kaniko](#kaniko)
    - [Hadolint](#hadolint)
    - [docker-compose-viz](#docker-compose-viz)
- [Why not x?](#why-not-x)
    - [Why aioli and not x?](#why-aioli-and-not-x)
    - [Why Docker swarm mode and not x?](#why-docker-swarm-mode-and-not-x)
    - [Why HAProxy and not x for load balancing?](#why-haproxy-and-not-x-for-load-balancing)
    - [Why nginx and not x for web server / reverse proxy?](#why-nginx-and-not-x-for-web-server-reverse-proxy)
    - [Why Ansible and not x for provisioning?](#why-ansible-and-not-x-for-provisioning)
- [Hacking Documentation](#hacking-documentation)
    - [Refreshing Table of Contents](#refreshing-table-of-contents)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

Happy hacking!

## Project Structure

An overview of a simplified directory structure can be helpful.

```
.
├── containers        Dockerfiles to be built by CI, where hacky stuff should go
│   ├── debug         handy when developing/debugging stacks
│   ├── nix-shell     image with developer tools; linters etc. installed
│   └── sshd          sshd image to be used as ansible target during CI
├── CONTRIBUTING.md
└── group_vars        Ansible varibles
    ├── all
    └── test.yml      Varibales that override other variables when testing
├── Makefile          Used for linting and syntax check of Ansible stuff
├── playbook.yml
├── README.md
├── roles
│   ├── admins        Sets up system users and ssh-keys
│   ├── docker        Installs docker deps and starts a single node swarm
│   ├── nextcloud     Indicates nextcloud should be deployed and/or updated
│   ├── prep          Prepares an empty list wher stacks can indicate
│   └── sshd          Hardens ssh, only allows key authentication etc.
├── scripts           Various scripts
│   ├── apply_vars.py More on this later
│   ├── clean.sh      Cleans up after prep.sh
│   └── prep.sh       Prepares local system for development
├── shell.nix         Used to install packages for development
└── stacks
    ├── debug         handy when developing/debugging stacks
    └── Makefile      Prepares and cleans for local developement
```


## Development Packages

### Adding a Package Used During Development

1. Search for package name [here](https://nixos.org/nixos/packages.html).
2. Add package name to list in `containers/nix-shell/shell.nix`

## Hacking Ansible

### Modifying/adding an Ansible role

1. `$ nix-shell` or install packages otherwise, but beware of version incompatibilities.

2. `$ vim/emacs/heresy $file`

3. `make check`

4. `vagrant up` OR if no changes to docker stuff (as to avoid docker-in-docker which would require `--privileged` on runners) `$ gitlab-runner exec docker ansible_test`.

5. Submit MR.

## Hacking CI

A short an simplified description of the current CI pipeline:

1. Check syntax and lint.
2. Build docker images and push them to the gitlab docker registry with the `eval_` prefix.
3. Run tests on `eval_` images.
4. Tag `eval_`images as `latest` if they pass the tests.

Anchors are used instead of `extend:` due to [this](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/3794).

Example(s) of running locally:

```bash
$ gitlab-runner exec docker ansible_test
```

Below is currently broken, but maybe worth playing with:

```bash
$ gitlab-runner exec virtualbox --virtualbox-disable-snapshots --virtualbox-base-name aioli-vm-debian-10-nixos-19.09 --ssh-user root --ssh-password root stack_nextcloud
```

### Kaniko

Example(s) of running locally:

```bash
$ docker run -v $(pwd):/workspace gcr.io/kaniko-project/executor:debug \
  --dockerfile=haproxy/Dockerfile \
  --context=/workspace --no-push
```

### Hadolint

Example(s) of running locally:

```bash
$ docker run --rm -i hadolint/hadolint < Dockerfile
```

### docker-compose-viz

Example(s) of running locally:

```bash
$ mkdir output && chmod output 777
$ docker run --rm -it --name dcv -v $(pwd):/input pmsipilot/docker-compose-viz render -m image stack.yml
```

[Ansible NAS]: https://github.com/davestephens/ansible-nas
[sovereign]: https://github.com/sovereign/sovereign
[HomelabOS]: https://homelabos.com/
[1]: https://github.com/davestephens/ansible-nas/blob/master/tasks/traefik.yml
[2]: https://gitlab.com/search?utf8=%E2%9C%93&snippets=&scope=&repository_ref=&search=docker.sock&project_id=6853087
[kubernetes]: https://kubernetes.io/
[traefik]: https://docs.traefik.io/
[nginx]: https://nginx.org/
[NixOps]: https://github.com/NixOS/nixops

## Why not x?

This seeks to explain aioli's design choices.

There will be questions, and a list of alternatives we are aware of for each.
Below each item, the reason they aren't a great fit is written.
Reasons that could be addressed via upstreaming
(e.g. declarative configuration of Nextcloud apps) are left out.

Please keep in mind that this is all at the time writing and there is nothing,
as far as we know, inherently wrong with these projects.
On the contrary, they serve as great inspiration.

Please raise an issue and/or create a merge request if you spot any errors.

### Why aioli and not x?

There are other cool projects similar to aioli.

- [Ansible NAS]
Exposes the Docker socket to traefik, which increases the impact
in case of a vulnerability being exploited in traefik [[1]].

- [sovereign]
Every service runs as a bare metal process, which increases the impact
in case of a vulnerability in one the hosted services.

- [HomelabOS]
Exposes the Docker socket to traefik, which increases the impact
in case of a vulnerability being exploited in traefik [[2]].


### Why Docker swarm mode and not x?

We are familiar with Docker swarm mode.

- [kubernetes]
We haven't thouroughly evaluated kubernetes for this purpose.
Docker swarm mode seems to work fine though.


### Why HAProxy and not x for load balancing?

HAProxy has DNS based autodiscovery and great documentation covering it.

- [traefik]
Would require exposing the docker socket to traefik.

- [nginx]
We haven't thouroughly evaluated nginx for this purpose.
HAProxy seems to work fine though.

### Why nginx and not x for web server / reverse proxy?
We are familiar with nginx for this purpose.

### Why Ansible and not x for provisioning?
We are familiar with Ansible.

- [NixOps]
NixOps uses stores state on the deploying machine.
NixOS does not have a swarm mode module as far as we know.
We haven't thouroughly evaluated NixOps for this purpose.


## Hacking Documentation

### Refreshing Table of Contents

```
docker run --net none --rm -it --entrypoint "doctoc" -v ~/path/to/aioli-infra/:/usr/src jorgeandrada/doctoc README.md --gitlab --notitle
```
